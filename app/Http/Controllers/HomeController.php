<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use Auth;
use App\User;
use PDF;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function mail(){
        $name = Auth::user()->name;
        $email = Auth::user()->email;

        $users = User::get();
        $pdf = PDF::loadView('user.pdf', ['users' => $users]);
        Mail::to($email)->send(new SendMailable($name));
        return back();
    }
}
