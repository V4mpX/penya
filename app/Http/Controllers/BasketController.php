<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Session;
use Carbon\Carbon;
use App\Order;

class BasketController extends Controller
{

    public function index(Request $request)
    {
        $total = 0;
        $basket = $request->session()->get('basket');
        if(! $basket) {
            $basket = [];
        }
        return view('basket.index', ['products' => $basket],['total' => $total]);
    }

    public function store(Request $request,$id)
    {
        $product = Product::findOrFail($id);
        $basket = $request->session()->get('basket');

        if($basket == null) {
            $basket = array();
        }

        $position = -1;
        foreach ($basket as $key => $item) {
            if ($item->id == $id) {
                $position = $key;
                $item->cantidad += 1;
                break;
            }
        }

        if($position == -1){
            $product->cantidad = 1;
            $request->session()->push('basket', $product);
        }

        return redirect('/basket');
    }

    public function flush(Request $request)
    {
        $request->session()->forget('basket');
        return back();
    }

    public function pull(Request $request,$id){
        //TODO
        $product = Product::findOrFail($id);
        $basket = $request->session()->get('basket');

        $value = $request->session()->pull('basket', $product);

        return back();
    }


    public function SaveBasket(Request $request){

        $productosSesion = $request->session()->get('basket');
        $date = Carbon::today();

        $order = New Order;
        $order->user_id = Auth()->user()->id;
        $order->date = $date;
        $order->paid = 0;
        $order->save();

        foreach ($productosSesion as $product) {
            $order->products()->attach($product->id,[
            'order_id' => $order->id,
            'price' => $product->price,
            'quantity' => $product->cantidad,
            ]);
        }

        $request->session()->forget('basket');
        return back();
    }

    public function contadorTotal(){
        //TODO
    }
}
