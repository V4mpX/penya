<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;

class RoleController extends Controller
{
    public function __construct(){

        $this->middleware('auth');
    }

    public function index()
    {
        $this->authorize('index', Role::class);
        $roles = Role::all();
        return view('role.index', ['roles' => $roles]);
    }

    public function show($id)
    {
        $role = Role::with('users')->findOrFail($id);

        $this->authorize('view', $role);
        return view('role.show', ['role' => $role]);
        //$role = Role::findOrFail($id);
        //return $role;
    }
}
