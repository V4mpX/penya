<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use PDF;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('index', User::class);
        // $users = User::all();
        $users = User::paginate(10);
        // return $users;
        return view('user.index', ['users' => $users]);
        //busca el fichero (uno de los dos):
        // /resources/views/user/index.php
        // /resources/views/user/index.blade.php
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validacion:
        $rules = [
            'name' => 'required|max:255|min:3',
            'email' => 'required|unique:users|max:255|email',
            'password' => 'required|max:255',
            // 'color' => 'required',
            // 'player' => 'required|max:255|min:5',
        ];

        $request->validate($rules);


        // $user = new User();
        // $user->name = $request->input('name');
        // $user->email = $request->input('email');
        // $user->password = bcrypt($request->input('password'));

        // $user->remember_token = str_random(10);
        // $user->save();

        // //opcion2
        // $user = User::create($request->all());

        //opcion3
        $user = new User();
        $user->fill($request->all());
        $user->password = bcrypt($user->password);
        $user->save();

        return redirect('/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        // $user = User::find($id);
        // if ($user == null) {
        //     abort(404, 'Prohibido!!!!');
        //     // response()->view('errors.404', [], 404);
        // }
        // $user = User::findOrFail($id);
        return view('user.show', [
            'user' => $user,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $this->authorize('update', $user);

        return view('user.edit', ['user' => $user]);
    }

    //2º metodo posible
    public function edit2($id)
    {
        $currentUser = \Auth::user();
        $user = User::findOrFail($id);
        if($currentUser->can('update', $user)){
            return "Adelante";
        }else{
            return back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $rules = [
            'name' => 'required|max:255|min:4',
            'email' => "required|unique:users,email,$id,id|max:255|email",
        ];

        $request->validate($rules);




        $user = User::findOrFail($id);
        $user->fill($request->all());
        $user->save();


        return redirect('/users/' . $user->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $this->authorize('delete',$user);
        $user->delete();

        //User::destroy($id);

        return back();
    }

    public function especial()
    {
        $users = User::where('id', '>=', 15)
            ->where('id', '<=', 20)
            ->get();

        dd($users);
        return "especial";
        return redirect('/users');
        // return "Especial";
    }

    public function exportPDF()
    {
      $users = User::get();
      $pdf = PDF::loadView('user.pdf', ['users' => $users]);
      $pdf->save(storage_path().'_filename.pdf');
      return $pdf->download('users.pdf');
    }
}
