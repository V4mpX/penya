<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'product_id', 'quantity','price'
    ];

    public function products()
    {
        return $this->belongsToMany(\App\Product::class)->withPivot('quantity');
    }

    public function total(){
        $total = 0;
        foreach ($this->products as $product) {
            $total += $product->price * $product->quantity;
        }
    }
}
