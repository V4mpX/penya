<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'price','cathegory_id'
    ];

    public function cathegory()
    {
        return $this->belongsTo(\App\Cathegory::class);
    }
}
