@extends('layouts.app')

@section('content')
    <h1>Lista de productos</h1>
    <a href="/products/create">Nuevo</a>
    <table class="table table-striped">
      <thead>
        <tr>
          <th>Nombre</th>
          <th>Precio</th>
          <th>Categoria</th>
          <th>Acciones</th>
        </tr>
      </thead>
      <tbody>
    @forelse ($products as $product)
        <tr>
          <td><a href="/products/{{ $product->id }}">{{ $product->name }}</a></td>
          <td>{{ $product->price }}</td>
          <td><a class="btn btn-info" href="/cathegories/{{ $product->cathegory->id }}">{{ $product->cathegory->name }}</a></td>
          <td>
            <div class="btn-group">
              <a class="btn btn-primary" href="/products/{{ $product->id }}">Ver</a>
              <a class="btn btn-success" href="/products/{{ $product->id }}/edit">Editar</a>
              <form method="post" action="/products/{{ $product->id }}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="DELETE">
                <input class="btn btn-danger" type="submit" value="borrar">
              </form>
              <a class="btn btn-warning" href="/basket/{{$product->id}} ">Añadir carrito</a>
            </div>
          </td>
        </tr>
    @empty
        <li>No hay productos</li>
    @endforelse
    </tbody>
    </table>

    {{ $products->render() }}
@endsection
