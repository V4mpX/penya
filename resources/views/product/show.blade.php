@extends('layouts.app')

@section('title', 'Productos')

@section('content')

    <h1>
        Este es el detalle del producto <?php echo $product->id ?>
    </h1>
    <a href="/products/{{ $product->id }}/edit">Editar</a>

    <ul>
        <li>Nombre: {{ $product->name }}</li>
        <li>Precio: {{ $product->price }}</li>
        <li>ID Categoria: {{ $product->cathegory_id }}</li>
    </ul>
    <a href="/products/">volver</a>
@endsection

