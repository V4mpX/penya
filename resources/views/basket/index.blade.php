@extends('layouts.app')

@section('content')
    <h1>Cesta</h1>
    <table class="table table-striped">
      <thead>
        <tr>
          <th>Nombre</th>
          <th>Cantidad</th>
          <th>Precio</th>
          <th>Borrar</th>
        </tr>
      </thead>
      <tbody>
    @forelse ($products as $product)
        <tr>
            <td>{{ $product->name }}</td>
            <td>{{ $product->cantidad }}</td>
            <td>{{$product->price}}</td>
            <td><form method="post" action="/basket/{{ $product->id }}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="DELETE">
                <input class="btn btn-danger" type="submit" value="borrar">
              </form>
            </td>
            {{! $total += $product->cantidad * $product->price }}
        </tr>
    @empty
        <tr><td colspan="4">No hay productos!!</td></tr>
    @endforelse
    @if ($total > 0)
        <tr>
          <td></td>
          <td>Total:</td>
          <td colspan="2">{{ $total }} €</td>
        </tr>
    @endif
    </tbody>
    </table>

  <a class="btn btn-success" href="/basket/saveBasket">Guardar Pedido</a>
  <a class="btn btn-danger" href="/basket/flush">Vaciar</a>
@endsection
