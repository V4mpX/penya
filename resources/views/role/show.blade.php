@extends('layouts.app')

@section('title', 'Roles')

@section('content')

    <h1>
        Usuarios dentro del rol {{ $role->name }}
    </h1>

    <ul>
        @foreach($role->users as $user)
        <li>{{ $user->name }}</li>
        @endforeach
    </ul>


    <a href="/roles/">volver</a>
@endsection

