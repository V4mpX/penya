@extends('layouts.app')

@section('content')
    <h1>Lista de roles</h1>
    <table class="table table-striped">
      <thead>
        <tr>
          <th>Nombre</th>
          <th>Acciones</th>
        </tr>
      </thead>
      <tbody>
    @forelse ($roles as $role)
        <tr>
            <td><a href="/roles/{{ $role->id }}">{{ $role->name }}</a></td>
            <td><a class="btn btn-primary" href="/roles/{{ $role->id }}">Ver</a></td>
        </tr>
    @empty
        <li>No hay roles</li>
    @endforelse
    </tbody>
    </table>

@endsection
