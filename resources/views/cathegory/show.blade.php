@extends('layouts.app')

@section('title', 'Categorias')

@section('content')

    <h1>
        Este es el detalle de la categoria {{ $cathegory->name }}
    </h1>
    <p>Productos dentro:</p>
    <ul>
        @foreach($cathegory->products as $product)
        <li>{{ $product->name }}</li>
        @endforeach
    </ul>
    <a href="/cathegories/">volver</a>
@endsection

