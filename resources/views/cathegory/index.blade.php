@extends('layouts.app')

@section('content')
    <h1>Lista de categorias</h1>
    <a href="/cathegories/create">Nuevo</a>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Acciones</th>
            </tr>
            </thead>
        <tbody>
        @forelse ($cathegories as $cathegory)
            <tr>
                <td><a href="/cathegories/{{ $cathegory->id }}">{{ $cathegory->name }}</a></td>
                <td>
                    <div class="btn-group">
                    <a class="btn btn-primary" href="/cathegories/{{ $cathegory->id }}">Ver</a>
                    <a class="btn btn-success" href="/cathegories/{{ $cathegory->id }}/edit">Editar</a>
                    <form method="post" action="/cathegories/{{ $cathegory->id }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="DELETE">
                        <input class="btn btn-danger" type="submit" value="borrar">
                    </form>
                    </div>
                </td>
            </tr>
        @empty
            <li>No hay categorias</li>
        @endforelse
        </tbody>
    </table>

    {{ $cathegories->render() }}
@endsection
