@extends('layouts.app')

@section('content')
    <h1>Grupo de usuarios</h1>
    <table class="table table-striped">
      <thead>
        <tr>
          <th>Nombre</th>
          <th>Email</th>
          <th>Rol</th>
        </tr>
      </thead>
      <tbody>
    @forelse ($users as $user)
        <tr>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td>{{$user->role->name}}</td>
        </tr>
    @empty
        <tr><td colspan="3">No hay usuarios!!</td></tr>
    @endforelse
    </tbody>
    </table>
    <a href="/groups/flush">Vaciar</a>
@endsection
