@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Tablón</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if (Auth::check())
                        Estás logueado! Ya puedes ver las funciones.
                    @else
                        Hola invitado! Logueate para ver más funciones.
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
