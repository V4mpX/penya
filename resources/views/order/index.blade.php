@extends('layouts.app')

@section('content')
    <h1>Pedidos</h1>

    <table class="table table-striped">
      <thead>
        <tr>
            <th>USER ID</th>
            <th>Pagado</th>
            <th>Fecha</th>
            <th>Accion</th>
        </tr>
      </thead>
      <tbody>
    @forelse ($orders as $order)
        <tr>
            <td>{{$order->user_id}}</td>
            <td>{{$order->paid}}</td>
            <td>{{$order->date}}</td>
            <td>confirmar pago</td>
        </tr>
    @empty
        <tr colspan="4">No hay pedidos!!</tr>
    @endforelse
    </tbody>
    {{ $orders->render() }}

@endsection
