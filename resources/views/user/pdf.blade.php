<<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>PDF Usuarios</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style>
    #users {
      border-collapse: collapse;
      width: 100%;
    }

    #users td, #users th {
      border: 1px solid #ddd;
      padding: 8px;
    }

    #users tr:nth-child(even){background-color: #f2f2f2;}

    #users th {
      padding-top: 12px;
      padding-bottom: 12px;
      text-align: left;
      background-color: #ADD8E6;
      color: white;
    }
  </style>
</head>
<body>
  <h1 style="background-color: black; color: white">Lista de Usuarios</h1>
  <table id="users">
    <thead>
      <tr>
        <th>ID</th>
        <th>Nombre</th>
        <th>Email</th>
        <th>Rol</th>
      </tr>
    </thead>
    <tbody>
      @foreach($users as $user)
        <tr>
          <td>{{ $user->id }}</td>
          <td>{{ $user->name }}</td>
          <td>{{ $user->email }}</td>
          <td>{{$user->role->name}}</td>
        </tr>
      @endforeach
    </tbody>
  </table>
</body>
</html> 
