@extends('layouts.app')

@section('content')
    <h1>Lista de usuarios</h1>
    <a href="/users/create">Nuevo</a>
    <br>
    <br>
    <table class="table table-striped">
      <thead>
        <tr>
          <th>Nombre</th>
          <th>Email</th>
          <th>Rol</th>
          <th>Acciones</th>
        </tr>
      </thead>
      <tbody>
    @forelse ($users as $user)
        <tr>
            <td><a href="/users/{{ $user->id }}">{{ $user->name }}</a></td>
            <td>{{ $user->email }}</td>
            <td><a class="btn btn-info" href="/roles/{{$user->role->id}}">{{$user->role->name}}</a></td>
            <td>
              <div class="btn-group">
                <a class="btn btn-primary" href="/users/{{ $user->id }}">Ver</a>
                @can('update', $user)
                <a class="btn btn-success" href="/users/{{ $user->id }}/edit">Editar</a>
                @endcan
                @can('delete', $user)
                <form method="post" action="/users/{{ $user->id }}">
                  {{ csrf_field() }}
                  <input type="hidden" name="_method" value="DELETE">
                  <input class="btn btn-danger" type="submit" value="borrar">
                </form>
                @endcan
                <!--<a class="btn btn-warning" href="/groups/{{$user->id}} ">Guardar</a>-->
              </div>
            </td>
        </tr>
    @empty
        <li>No hay usuarios!!</li>
    @endforelse
    </tbody>
    </table>

    {{ $users->render() }}
    <br>
    <a class="btn  btn-danger" href="/users/pdf">Export PDF</a>
    <br>
    <a class="btn  btn-danger" href="/users/send">Enviarte el PDF por correo</a>

@endsection
