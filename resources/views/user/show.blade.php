@extends('layouts.app')

@section('title', 'Usuarios')

@section('content')

    <h1>
        Este es el detalle del usuario <?php echo $user->id ?>
    </h1>

    <ul>
        <li>Nombre: {{ $user->name }}</li>
        <li>Email: {{ $user->email }}</li>
        <li>Registrado: {{$user->created_at}}</li>
    </ul>
    @can('update', $user)
        <a class="btn btn-success" href="/users/{{ $user->id }}/edit">Editar</a>
    @endcan
    <br>
    <a href="/users/">volver</a>
@endsection

