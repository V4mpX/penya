<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('users', 'UserController@index')->name('usuarios');
// Route::get('users/{id}', 'UserController@show');
// Route::get('users/{id}/edit', 'UserController@edit');
// Route::get('users/create', 'UserController@create');
// Route::put('users/{id}', 'UserController@update');
// Route::delete('users/{id}', 'UserController@destroy');
// Route::post('users', 'UserController@store');
// Route::get('users/create', 'UserController@create');

//Ruta de tipo resource: equivale a las 7 rutas REST
//Ruta especial antes que resource, si no "show...."
Route::get('users/especial', 'UserController@especial');
Route::get('users/pdf', 'UserController@exportPDF');
Route::get('/users/send', 'HomeController@mail');
Route::get('users/{id}/edit2', 'UserController@edit2');
Route::resource('users', 'UserController');
Route::resource('products', 'ProductController');
Route::resource('cathegories', 'CathegoryController');
Route::resource('roles', 'RoleController');
Route::get('/redirect/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('/callback/{provider}', 'Auth\LoginController@handleProviderCallback');


////////////////////////////////////////////
//Ejemplos de rutas con funciones anónimas:
////////////////////////////////////////////

Route::get('usuarios/{id}', function ($id) {
   return "Detalle del usuario $id";
});


Route::get('usuarios/{id}', function ($id) {
   return "Detalle del usuario $id";
})->where('id', '[0-9]+');

Route::get('usuarios/{id}/{name?}', function ($id, $name=null) {
    if($name) {
        return "Detalle del usuario $id. El nombre es $name";
    } else {
        return "Detalle del usuario $id. Anónimo";
    }
});

Route::get('productos/{id}', function ($id) {
   return "Detalle del producto $id";
});

Route::get('cathegories/{id}', function ($id) {
   return "Detalle de la categoria $id";
});





Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/groups', 'GroupController@index');
Route::get('/groups/flush', 'GroupController@flush');
Route::get('/groups/{id}', 'GroupController@addUser');

Route::get('/basket', 'BasketController@index');
Route::get('/basket/flush', 'BasketController@flush');
Route::get('/basket/saveBasket', 'BasketController@saveBasket');
Route::get('/basket/{id}', 'BasketController@store');
Route::delete('/basket/{id}', 'BasketController@pull');


Route::get('/orders', 'OrderController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
