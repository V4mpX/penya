<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */

    public function test_lista_de_productos()
    {
      $response = $this->get('/products');
      $response->assertStatus(200);
      $response->assertDontSee('No hay productos');
      $response->assertSee('Lista de productos');
    }

    public function test_metodo_show_prod_1()
    {
      $this->seed('RolesTableSeeder');

      factory(User::class)->create([
        'id' => 1,
        'name' => 'Producto',
        'price' => '1',
        'cathegory_id' => '1'
      ]);

      $response = $this->get('/products/1');
      $response->assertStatus(200);
      $response->assertSee('detalle del producto 1');
      $response->assertSee('Producto');
      $response->assertSee('1');
      $response->assertSee('1');
    }

    public function test_metodo_show_prod_inexistente()
    {
      $response = $this->get('/products/100000');
      $response->assertStatus(404);
    }

    public function test_store_prod()
    {
        $this->seed('RolesTableSeeder');

        $this->post('/product', [
            'name' => 'Producto',
            'price' => '1',
            'cathegory_id' => '1'
        ])->assertRedirect('products');


        $this->assertDatabaseHas('product', [
            'name' => 'Producto',
            'price' => '1',
            'cathegory_id' => '1'
        ]);

        $this->assertCredentials([
            'name' => 'Producto',
            'price' => '1',
            'cathegory_id' => '1'
        ]);
    }
}
